import $ from "jquery";
import {Promise} from "es6-promise";

const API_VERSION = 'v4';

function setError(input) {
    input.addClass('error');
}

function getRepository(link, token) {
    link = link.trim();
    if (!link) {
        return null;
    }
    const data = link.split('/');

    if (data.length < 4) {
        return null;
    }

    return {
        base_url: data[0] + "//" + data[2] + '/api/' + API_VERSION + '/',
        user_id: data[3],
        token: token,
        project: {
            name: data[4],
        },

        _send(url, return_xhr = false) {
            return new Promise((res, rej) => {
                $.ajax({
                    url: url,
                    method: 'GET',
                    headers: {
                        'Private-Token': this.token
                    },
                    success: (response, status, xhr) => {
                        if (return_xhr) {
                            res({response, xhr});
                        } else {
                            res(response);
                        }
                    },
                    error: (xhr, status, err) => {
                        rej({xhr, status, err});
                    }
                })
            });
        },

        getProjects(visibility) {
            let url = this.base_url + 'users/' + this.user_id + '/projects';

            if (visibility) {
                url += `?visibility=${visibility}`;
            }
            return this._send(url);
        },

        getBranchCommits(branch) {
            return new Promise((res, rej) => {
                let url = this.base_url + 'projects/' + encodeURIComponent(this.user_id + '/' + this.project.name);
                const commit_url_base = `${url}/repository/commits?ref_name=${branch.name}&per_page=100`;
                let page = 1;
                let branch_commits = [];

                this._send(commit_url_base + `&page=${page}`, true)
                    .then((response) => {
                        const commits = response.response;
                        const rest_pages = response.xhr.getResponseHeader('x-total-pages') - page;
                        // console.log('total-pages: ', rest_pages + page);
                        branch_commits = branch_commits.concat(commits);

                        if (rest_pages > 0) {
                            let promises = [];
                            for (let i = 0; i < rest_pages; i++) {
                                promises.push(this._send(commit_url_base + `&page=${page + i + 1}`))
                            }
                            Promise
                                .all(promises)
                                .then((values) => {
                                    for (const val of values) {
                                        branch_commits = branch_commits.concat(val);
                                    }
                                    res(branch_commits);
                                });
                        } else {
                            res(branch_commits);
                        }
                    }, (error) => {
                        rej(error);
                    });
            });
        },

        getAllCommits() {
            return new Promise((res, rej) => {
                let url = this.base_url + 'projects/' + encodeURIComponent(this.user_id + '/' + this.project.name);
                this._send(url + '/repository/branches')
                    .then((branches) => {
                        const promises = [];
                        for (const branch of branches) {
                            promises.push(this.getBranchCommits(branch));
                        }
                        let commits = [];
                        Promise
                            .all(promises)
                            .then((data) => {
                                for (const branch_commits of data) {
                                    commits = commits.concat(branch_commits);
                                }
                                // console.log('all commits: ', commits);
                                res(commits
                                    .filter((value, index, self) => {
                                        return index === self.findIndex(t => t.short_id === value.short_id);
                                    })
                                    .sort((a, b) => {
                                        return new Date(b.committed_date) - new Date(a.committed_date);
                                    }));
                            });
                    }, (error) => {
                        rej(error);
                    });
            });
        },

        getProjectInfo() {
            return new Promise((res, rej) => {
                let url = this.base_url + 'projects/' + encodeURIComponent(this.user_id + '/' + this.project.name);
                Promise.all([
                    this._send(url),
                    this.getAllCommits(),
                ]).then((data) => {
                    const repo_info = {
                        name: data[0].name,
                        description: data[0].description,
                        created_at: new Date(data[0].created_at),
                        last_activity_at: new Date(data[0].last_activity_at),
                        commits: {
                            list: data[1],
                            count: data[1].length,
                        },
                    };
                    res(repo_info);
                }, (error) => {
                    rej(error);
                })

            });
        }

    };
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        let cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}

function createCommit(commit) {
    const $commit = $(document.createElement('div'));
    $commit.addClass('Commit');
    $commit.append(`<h2 class="title"><span class="date">${new Date(commit.committed_date)}</span> ${commit.short_id}</h2>`);
    $commit.append(`<pre>${JSON.stringify(commit, null, 4)}</pre>`);
    return $commit;
}

function createProjectBlock(project) {
    const $block = $(document.createElement('div'));
    $block.addClass('ProjectBlock');
    $block.append(`<h2 class="title">${project.name}</h2>`);
    $block.append(`<p class="desc">${project.description ? project.description : 'Нет описания'}</p>`);
    $block.append(`<p class="date">Первый коммит: ${project.created_at}</p>`);
    $block.append(`<p class="date">Последняя активность: ${project.last_activity_at}</p>`);
    $block.append(`<p>Общее количество коммитов: ${project.commits.count}</p>`);

    const $commits = $(document.createElement('div'));
    $commits.addClass('CommitsBlock');
    for (const commit of project.commits.list) {
        $commits.append(createCommit(commit));
    }
    $block.append($commits);

    return $block;
}

function setButtons() {
    const $repo_link = $('input#repo-name');
    const $private_token = $('input#private-token');

    $('#btn-get-info').on('click', () => {
        const repo = getRepository($repo_link.val(), $private_token.val());

        if (!repo) {
            setError($repo_link);
        } else {
            repo.getProjectInfo().then((info) => {
                console.log(info);
                const $projects_container = $('.Response');
                $projects_container.empty();
                $projects_container.append(createProjectBlock(info));
            }, (error) => {
                console.log(error);
            });
        }

        // const visibility = $('input[name="visibility"]:checked').val();

        // repo.getProjects(visibility).then((projects) => {
        //     console.log(projects);
        //     const $projects_container = $('.Response');
        //     $projects_container.empty();
        //     for (const p of projects) {
        //         console.log(p);
        //         $projects_container.append(createProjectBlock(p));
        //     }
        // });


    });
}

setButtons();