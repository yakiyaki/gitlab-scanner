var gulp = require('gulp');
var stylus = require('gulp-stylus');
var concatCss = require('gulp-concat-css');
var del = require('del');
var webpack = require("webpack");
var gwebpack = require("gulp-webpack");

var BUILD_DEST = 'src/_build';

gulp.task('checker:clean', () => {
    return del.sync([BUILD_DEST], {force: true});
});

gulp.task('checker:styles', function () {
    gulp.src('src/css/**/*')
        .pipe(stylus())
        .pipe(concatCss('bundle.css'))
        .pipe(gulp.dest(BUILD_DEST))
});

gulp.task('checker:bundle', () => {
    return gulp.src('src/index.js')
        .pipe(gwebpack(require('./webpack.config.js'), webpack))
        .pipe(gulp.dest(BUILD_DEST));
});

gulp.task('checker:assemble', ['checker:clean', 'checker:styles', 'checker:bundle'], function () {

});